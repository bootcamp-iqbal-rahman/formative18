const dreamerTop = document.querySelector(".dreamer__top");
const dreamerBot = document.querySelector(".dreamer__bottom");

const topColor = "rgb(85, 162, 166)";
const botColor = "#515050";

if (dreamerTop && dreamerBot) {
  dreamerTop.addEventListener("mouseover", () => {
    dreamerTop.style.backgroundColor = botColor;
    dreamerBot.style.backgroundColor = topColor;
  });

  dreamerBot.addEventListener("mouseover", () => {
    dreamerBot.style.backgroundColor = topColor;
    dreamerTop.style.backgroundColor = botColor;
  });

  dreamerTop.addEventListener("mouseout", () => {
    dreamerTop.style.backgroundColor = topColor;
    dreamerBot.style.backgroundColor = botColor;
  });

  dreamerBot.addEventListener("mouseout", () => {
    dreamerBot.style.backgroundColor = botColor;
    dreamerTop.style.backgroundColor = topColor;
  });
}
